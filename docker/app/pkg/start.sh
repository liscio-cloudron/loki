#!/bin/bash

set -eu

mkdir -p /app/data/runtime /app/data/config

if [[ ! -f /app/data/config/loki-local-config.yaml ]]; then
    echo "=> Creating config file on first run"
    cp /app/config/loki-local-config.yaml /app/data/config/loki-local-config.yaml
fi

chown -R cloudron:cloudron /app/data

echo "=> Starting Loki"
/usr/local/bin/gosu cloudron:cloudron /app/code/loki-linux-amd64 --config.file=/app/data/config/loki-local-config.yaml
