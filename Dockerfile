FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=2.4.1

# install Loki
RUN curl -O -L "https://github.com/grafana/loki/releases/download/v${VERSION}/loki-linux-amd64.zip"
RUN unzip "loki-linux-amd64.zip"
RUN chmod a+x "loki-linux-amd64"

COPY docker /

CMD [ "/app/pkg/start.sh" ]
